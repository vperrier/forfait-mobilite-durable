# Forfait Mobilité durable


## Description
The aim of this script is to compute the number of days of a calendar that are neither a mission nor a day off (Saturday, Sunday, public holiday or CASA day). This script is written in `python3`. You need an access to internet for using it. It was originally developed following the request to compute all the days per month on which a sustainable transport is used for being eligible with the `Forfait Mobilité durable`. 

## Usage
The script should be launched as follows
```code
python3 fmd.py
```
You need to answer six questions for launching the computation

### Name of the calendar 

```code
[1/6] Enter the name of the calendar to parse (it should look like firstname.name (e.g. georges.abitbol)) >
```

This is what appears in the middle of the URL of the calendar. For me, it is
`vincent.perrier`, and the URL of the calendar `https://zimbra.inria.fr/home/<b>vincent.perrier</b>@inria.fr/Calendar.ics`

### Login

```code
[2/6] Enter the login for accessing the account (e.g. gabitbol) >
```
This is your LDAP login that is used for accessing `zimbra`, `mattermost`, 
the `intranet`, etc... For me, it is `vperrier`.

### Password 

```code
[3/6] Enter your password (e.g. azerty123 ;-) ) >
```
This is your LDAP password that is used when accessing `zimbra`, `mattermost`, 
the `intranet`, etc... For me, it is `***********` 
(and no, it is not `azerty123`). 

### Year to parse

```code
[4/6] Enter the year to parse (e.g. 2023) >
```

The year on which ou wish to compute the days that are neither a day off nor a mission. 

### Recurrent day off for people working at 80%

```code
[5/6] If you work at 80%, Enter the day you do not work (0 for Monday, 1 for Tuesday, 2 for Wednesday, 3 for Thursday, 4 for Friday, -1 if you work at 100%) >  -1
```
If you work at 80%, the week days you do not work should be excluded. Enter
- 0 if you do not work on Monday
- 1 if you do not work on Tuesday
- 2 if you do not work on Wednesday
- 3 if you do not work on Thursday
- 4 if you do not work on Friday
- -1 if you work at 100%


### Dumping the full calendar

```code
[6/6] Do you want to dump the full calendar (0 for "No", 1 for "Yes")? >
```
If you want to get more information on the days that are day off, missions, etc..., the script will dump all the days that are in your calendar, and says whether this day was a mission, a day off, or nothing.

## Known problem

The "RTT Fixe" is put in your calendar at the beginning of the year via an email. This means that if you joined INRIA during the year, the "RTT Fixe" may not be included in your calendar, and so these days may be erroneously not counted as day off.

## Support

Any help request or feature request should be put in the `Issues` section of this gitlab page, and assign it to `@vperrier`


## License

A license for 100 lines of `python`, seriously?
