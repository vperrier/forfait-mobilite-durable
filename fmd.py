from icalendar import Calendar
import calendar
from datetime import date, timedelta
import requests
import sys
import getpass

calendarname = input('[1/6] Enter the name of the calendar to parse (it should look like firstname.name (e.g. georges.abitbol)) >  ')
login = input('[2/6] Enter the login for accessing the account (e.g. gabitbol) >  ')
password = getpass.getpass('[3/6] Enter your password (e.g. azerty123 ;-) ) >  ')
yearstr = input('[4/6] Enter the year to parse (e.g. 2023) >  ')
try:
    year = int(yearstr)
except ValueError:
    print("*** ERROR in question 4;\""+yearstr+"\" could not be converted into an integer")
    sys.exit()

dayoffstr  = input('[5/6] If you work at 80%, Enter the day you do not work (0 for Monday, 1 for Tuesday, 2 for Wednesday, 3 for Thursday, 4 for Friday, -1 if you work at 100%) >  ')
try:
    dayoff = int(dayoffstr)
except ValueError:
    print("*** ERROR in question 5;\""+dayoffstr+"\" could not be converted into an integer")
    sys.exit()
if ( ( dayoff < -1 ) or ( dayoff > 4 ) ):
    print("*** ERROR in question 5: the day off you entered is \""+dayoffstr+"\".")
    print("An integer between -1 and 4 is expected:")
    print("-1 if you work 100%")
    print("0 if you work at 80% and do not work on Monday")
    print("1 if you work at 80% and do not work on Tuesday")
    print("2 if you work at 80% and do not work on Wednesday")
    print("3 if you work at 80% and do not work on Thursday")
    print("4 if you work at 80% and do not work on Friday")
    print("Exiting")
    sys.exit()

dumpstr  = input('[6/6] Do you want to dump the full calendar (0 for \"No\", 1 for \"Yes\")? >  ')
try:
    dumpint = int(dumpstr)
except ValueError:
    print("*** ERROR in question 6;\""+dumpstr+"\" could not be converted into an integer")
    sys.exit()
assert( ( dumpint == 1  ) or ( dumpint == 0 ) )
dump = ( dumpint == 1)

#Get Zimbra calendar
MyCalendarURL = "https://zimbra.inria.fr/home/"+calendarname+"@inria.fr/Calendar.ics"
response = requests.get( MyCalendarURL, auth=(login,password) )

if( response.status_code != 200 ):
    print("ERROR: Failed to open \""+MyCalendarURL+"\".")
    print("with user name = \""+login+"\" \t and password = \""+password+"\".")
    print("Error status: ",response.status_code )
    print("Exiting")
    sys.exit()

MyCalendar = Calendar.from_ical( (response.content).decode('utf-8') )

#Get public calendar with public holidays
PublicHolidayURL = "https://etalab.github.io/jours-feries-france-data/ics/jours_feries_metropole.ics"
response2 = requests.get( PublicHolidayURL )
PublicHolidayCalendar = Calendar.from_ical( (response2.content).decode('utf-8') )

# Fill a list of the events of the year, beginning by a list full of empty string
start_date = date(year, 1, 1)
end_date = date(year+1, 1, 1)
allDays = [ start_date + timedelta(n) for n in range(int((end_date - start_date).days))]
busy = [ "" for n in allDays]

## First, add Saturday and Sunday
for i in range( len(allDays) ):
    thisDay = allDays[i]
    if ( ( thisDay.weekday() >= 5 ) or ( thisDay.weekday() == dayoff ) ):
        busy[i] += calendar.day_name[ thisDay.weekday() ]
        if ( thisDay.weekday() >= 5 ):
            busy[i] += "-"
        else:
            busy[i] += " (80%)-"

##Second, add public holidays
for component in PublicHolidayCalendar.walk():
   if component.name == "VEVENT":
       dateTuple = (component.decoded("dtstart")).timetuple()
       if( dateTuple.tm_year == year ):
           index = dateTuple.tm_yday - 1
           busy[ index ] += "Public Holiday ("+component.get("SUMMARY")+")-"

##Third, add CONGES and Missions
for component in MyCalendar.walk():
   if component.name == "VEVENT":
       dateTuple = (component.decoded("dtstart")).timetuple()
       if( dateTuple.tm_year == year ):
           organizer = component.get("organizer")

           if ( ( organizer != None ) and ( "CN" in organizer.params ) and ( organizer.params["CN"] == "CASA") ):
               # conges/RTT fixe/Teletravail
               dateStart = (component.decoded("dtstart")).timetuple()
               dateEnd = (component.decoded("dtend")).timetuple()

               for index in range(dateStart.tm_yday-1,dateEnd.tm_yday):
                   busy[ index ] += "CASA ("+component.get("description")+")-"
           elif( ( organizer != None ) and ( "Portail Missions/GFD" in organizer) ):
               #Missions
               dateStart = (component.decoded("dtstart")).timetuple()
               dateEnd = (component.decoded("dtend")).timetuple()

               shiftStart = 0
               shiftEnd = 0
               if ( dateStart.tm_hour >= 12 ):
                   #if the mission starts after 12h
                   shiftStart = 1
               if ( dateEnd.tm_hour <= 13 ):
                   #if the mission stops before 13h
                   shiftEnd = -1
               for index in range(dateStart.tm_yday-1+shiftStart,dateEnd.tm_yday+shiftEnd):
                   busy[ index ] += "MISSION ("+component.get("description")+")-"

# The days I come with my bike are the ones which were not filled

## Fill a dictionary with 0 for each month
BikeDays = dict.fromkeys( [calendar.month_name[index] for index in range(1,13)] , 0 )

## For each day for which busy is an empty string, add 1 to the matching month
for index in range( len(allDays) ):
    thisDay = allDays[index]
    if( dump ):
        if( thisDay.day == 1 ):
            print(20*"-" + "  " + calendar.month_name[ thisDay.month] + "  " + 20*"-")
        print(str(thisDay) + "\t" + busy[ index ] )
    if ( busy[ index ] == '' ):
        BikeDays[ calendar.month_name[ thisDay.month] ] += 1

## Print the number of days per month
print(20*"-")
for month in BikeDays:
    print( month + ": " + str(BikeDays[month]) )
print(20*"-")

## Print the total number of bike days
print("Total = ",sum(BikeDays.values()))
